package com.briup.bean;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.format.NumberFormat;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import lombok.Data;

import java.util.Date;

/**
 * 读取数据的封装对象
 */
@Data
public class DemoData {
    //自定义的转化器
    @ExcelProperty(value = "姓名")
    private String name;

    @ColumnWidth(50)
    @DateTimeFormat("yyyy年MM月dd日 HH时mm分ss秒")
    @ExcelProperty("时间标题")
    private Date date;
    @NumberFormat("0.00")
    @ExcelProperty("数字标题")
    private Double doubleData;
}
