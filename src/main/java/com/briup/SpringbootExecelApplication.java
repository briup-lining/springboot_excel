package com.briup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootExecelApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootExecelApplication.class, args);
    }

}
