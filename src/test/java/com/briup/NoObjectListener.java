package com.briup;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

import java.util.Collection;
import java.util.Map;

public class NoObjectListener extends AnalysisEventListener<Map<Integer,Object>> {
    @Override
    public void invoke(Map<Integer, Object> data, AnalysisContext context) {
        System.out.println("每行数据："+data);
        Collection<Object> values = data.values();
        values.stream().forEach(value -> System.out.println(value.getClass()));
        //String类型时间---->Date类型 LocalDate类型
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {

    }
}
