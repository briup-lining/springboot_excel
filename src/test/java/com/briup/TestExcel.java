package com.briup;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.read.listener.PageReadListener;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.multipart.MultipartFile;

@SpringBootTest
public class TestExcel {
    @Autowired
    private DemoDataMapper mapper;

    @Test
    public void read(){
        String fileName = "F://demo.xlsx";
        EasyExcel.read(fileName, DemoData.class,new DemoDataListener(mapper)).sheet().doRead();
    }

    @Test
    public void readByJdk8(){
        //PageReadListener 将数据解析并封装到集合对象，提供处理集合数据逻辑
        String fileName = "F://demo.xlsx";
        EasyExcel.read(fileName, DemoData.class,
                new PageReadListener<DemoData>(list->{
                            //实现了Consumer接口中accept方法
                            mapper.insertList(list);
                        },5)).sheet().doRead();
    }
    @Test
    public void readData(){
        //不通过封装java对象，直接读取数据
        String fileName = "F://demo.xlsx";
        EasyExcel.read(fileName,new NoObjectListener()).sheet().doRead();
    }
    //通过网络方式读取数据 文件上传
    public void upload(MultipartFile file) throws Exception{
        EasyExcel.read(file.getInputStream(), DemoData.class,new DemoDataListener(mapper));
    }
    @Test
    public void read4(){
        String fileName = "F://demo.xlsx";
        EasyExcel.read(fileName, DemoData.class,new PageReadListener<DemoData>(list->{
            list.forEach(System.out::println);
        })).sheet("Sheet2").doRead();
    }

}

