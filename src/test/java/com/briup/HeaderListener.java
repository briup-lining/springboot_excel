package com.briup;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

import java.util.Map;

public class HeaderListener extends AnalysisEventListener {
    @Override
    public void invoke(Object data, AnalysisContext context) {

    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {

    }

    @Override
    public void invokeHead(Map headMap, AnalysisContext context) {
        System.out.println(headMap);
    }
}
