package com.briup;


import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.ReadConverterContext;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.alibaba.excel.metadata.property.ExcelContentProperty;

/**
 * 转化器 转化读 或者 写
 */
public class NameConverter implements Converter<String> {
    @Override
    public String convertToJavaData(ReadConverterContext<?> context) throws Exception {
        //将读取的名字转为大写
        return context.getReadCellData()
                .getStringValue().toUpperCase();
    }
    //重写

    @Override
    public WriteCellData<?> convertToExcelData(String value, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        return new WriteCellData<>(value.toUpperCase());
    }
}
