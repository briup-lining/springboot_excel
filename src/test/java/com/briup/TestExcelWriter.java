package com.briup;

import com.alibaba.excel.EasyExcel;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TestExcelWriter {

    public List<DemoData> data(){
        ArrayList<DemoData> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            //1.构造器
            //2.set方法 lombok 链式调用
            //3.builder
            DemoData data = new DemoData();
            data.setDate(new Date());
            data.setName("lisi");
            data.setDoubleData(200.00);
            list.add(data);
        }
        return list;
    }
    @Test
    public void write(){
        String fileName = "F://demo2.xlsx";
        EasyExcel.write(fileName, DemoData.class)
                .sheet("学生信息").doWrite(data());
    }

}
