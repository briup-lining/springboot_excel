package com.briup;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DemoDataMapper {
    public void insert(DemoData data){
        System.out.println("模拟插入数据："+data);
    };
    //insert into xxx values(),(),(),();
    public void insertList(List<DemoData> list){
        System.out.println("模拟批量插入："+list.size());
    }
}
